var flagTime = 1; //标志变量，1为时间有剩余，0为时间结束(时间结束判断)
//选择题题目获取
$.ajax({
    type: "get",
    url: "/showChoQst",
    dataType: "json",
    async: false, //同步请求
    cache: false,
    data: {},
    success: function(data) {
        var textForm = $("#danxuan");
        textForm.html("");
        var str = '<h3>一、选择题</h3><span class="tishi">(本小题每题3分，总分18。)</span>';
        $.each(data['result'], function(index, obj) {
            str += '<div class="co">'
            str += '<h4 onselectstart="return false" id="' + obj['choiceQstId'] + '">' + '<strong>' + obj['choiceQstId'] + '</strong>' + ' ' + '.' + ' ' + obj['choiceQstExp'] + '</h4>'
            str += '<input type="radio" name="optQuestion' + index + '" id="A' + index + '" value="A"><label for="A' + index + '">A.' + obj['a'] + '</label></br>'
            str += '<input type="radio" name="optQuestion' + index + '" id="B' + index + '" value="B"><label for="B' + index + '">B.' + obj['b'] + '</label></br>'
            if (obj['c']) { str += '<input type="radio" name="optQuestion' + index + '" id="C' + index + '" value="C"><label for="C' + index + '">C.' + obj['c'] + '</label></br>' }
            if (obj['d']) { str += '<input type="radio" name="optQuestion' + index + '" id="D' + index + '" value="D"><label for="D' + index + '">D.' + obj['d'] + '</label></br>' }
            str += '</div>'
        })
        textForm.html(str);
        textForm.append();
    },
    error: function() {
        alert("请求选择题失败");
    }
})
// 大题题目获取
$.ajax({
    type: "get",
    url: "/abq",
    async: false, //同步请求
    dataType: "json",
    data: {},
    success: function(data) {
        var textForm2 = $("#luoji");
        textForm2.html("");
        var str = "<h3>二、解答题</h3><span class=\"tishi\">(总分62)</span>";
        $.each(data['question'], function(index, obj) {
            str += '<div class="co2">'
            str += '<h4 onselectstart="return false"  id="' + obj['id'] + '">' + '<strong>' + obj['id'] + '</strong>' + ' ' + '.' + ' ' + obj['bigquestion'] + '</h4>'
            str += '<textarea name="anQuestion' + index + '" id="s' + index + '" cols="100" rows="10" value=""></textarea>'
            str += '</div>'

        })
        textForm2.html(str);
        textForm2.append()
    },
    error: function() {
        alert("请求解答题失败");
    }
})
window.onload = function() {
    ListA(); //答题情况清单
    time(); //计时器
    XS(); //返回顶部
    var tTime = setInterval(Answer, 500); //每隔1s获取一次题目答案


    //答题情况清单
    function ListA() {
        var List = $(".xList");
        var Danxuan = $(".co");
        var Luoji = $(".co2");
        List.html("");
        var str = '';
        $.each(Danxuan, function(index, obj) {
            var id = $(".co").eq(index).children("h4").attr("id");
            str += '<a href="#' + id + '" class="ti">' + id + '</a>'
        })
        $.each(Luoji, function(index, obj) {
            var id2 = $(".co2").eq(index).children("h4").attr("id");
            str += '<a href="#' + id2 + '" class="ti">' + id2 + '</a>'
        })
        str += '<div class="clearfloat "></div>';
        List.html(str);
        List.append();
    };

    // 倒计时
    function time() {
        var maxtime = 120 * 60;
        var Time = document.getElementById('time');
        var timer = setInterval(function CountDown() {
            if (maxtime >= 0) {
                var minutes = Math.floor(maxtime / 60);
                var seconds = maxtime % 60;
                var str = "距离结束还有" + minutes + "分" + seconds + "秒!";
                Time.innerHTML = str;
                if (maxtime == 1 * 60) { alert("还剩5分钟,注意把握时间！"); }

                --maxtime;
            } else {
                clearInterval(timer);
                clearInterval(tTime);
                flagTime = 0;
                ajaxSubmit();
                alert("时间到，结束!\n点击确认自动提交！");
            }
        }, 1000)
    }

    var arr = new Object(); //选择题答案
    var warr = new Array(); //大题答案
    //大题答案
    function objToArr(obj) {
        var arr = [];

        for (var i in obj) {
            var o = {};
            o[i] = obj[i];
            arr.push(o);
        }
        return arr;
    }

    function Answer() {
        var list = document.getElementsByClassName("ti");
        //console.log(list)
        var flagTip = 0; //标志变量，0为题目全部做完，1为未完成(题目是否为空的判断)
        var Danxuan = $(".co");
        $.each(Danxuan, function(index, obj) {
            var answer = $(".co").eq(index).children("input:checked").val();
            var titleName = $(".co").eq(index).children("input").attr("name");
            var id = $(".co").eq(index).children("h4").attr("id");
            if (answer != undefined) {

                arr[titleName] = answer;
                list[id-1].style.backgroundColor = 'green';

            } else {
                flagTip = 1;
                arr[titleName] = null;
                list[id-1].style.backgroundColor = 'white';

            }


        });
        //console.log(arr);
        var Luoji = $(".co2");
        warr.length = 0;
        $.each(Luoji, function(index, obj) {
            var answer = $(".co2").eq(index).children("textarea").val();
            //     var titleName = $(".co2").eq(index).children("textarea").attr("name");
            var id2 = $(".co2").eq(index).children("h4").attr("id");

            if (answer != undefined && answer != null && answer != '') {
                var wArray = { id: id2, wAnswer: answer };
                //warr[titleName] = wArray;
                warr.push(wArray);
                list[id2-1].style.backgroundColor = 'green';
            } else {
                flagTip = 1;
                var wArray = { id: id2, wAnswer: null };
                //warr[titleName] = wArray;
                warr.push(wArray);
                list[id2-1].style.backgroundColor = 'white';
            }
        })
        //console.log();
        return flagTip;
    }
    $("#submit").click(panDuan); //点击提交
    //提交时判断题目是否答完
    function panDuan() {
        if (flagTime == 1 && Answer()) {
            if (confirm("你还有题没有答完，是否确认提交")) {
                ajaxSubmit();
            }
        } else ajaxSubmit();
    };

    //ajax传答案
    function ajaxSubmit() {
        // console.log(arr);
        // console.log(warr);
        $.ajax({
            type: "post",
            url: "/getAns",
            contentType: "application/json",
            traditional: true,
            dataType: "json",
            async: false, //同步请求
            data: JSON.stringify({
                choiceAnswers: objToArr(arr),
                essayAnswers: warr
            }),
            success: function(msg) {
                if (msg) {
                    clearInterval(tTime);
                    alert("答案提交成功！");
                    window.location.replace("/ending");
                } else {
                    alert("答案提交失败!");
                }
            },
            error: function() {
                alert("答案提交请求失败!");
            }
        })
    };

    function XS() {
        //获取回到顶部的按钮
        var Btn = document.getElementById('ic');
        var timer = null;
        // 标识是否清除定时器，为了实现在回到顶部的过程中通过滚动一下鼠标实现清除定时器从而达到滚动的目的
        var isTop = true;
        // 获取页面可视区高度，从而判断返回顶部按钮是否出现
        var cHeight = document.documentElement.clientHeight;
        window.onscroll = function() {

            // 让返回顶部按钮在第二屏才开始出现
            var osTop = document.documentElement.scrollTop || document.body.scrollTop;
            if (osTop >= cHeight) {
                Btn.style.display = 'block';
            } else {
                Btn.style.display = 'none';
            }
            if (!isTop) {
                clearInterval(timer);
            }
            isTop = false;
        };
        Btn.onclick = function() {
            // 设置定时器
            timer = setInterval(function() {
                // 获取滚动条距离顶部的高度
                var osTop = document.documentElement.scrollTop || document.body.scrollTop;
                // 越滚越慢，设置成负数是为了防止减不到0
                var ispeed = Math.floor(-osTop / 3);
                document.documentElement.scrollTop = document.body.scrollTop = osTop + ispeed;
                isTop = true; // 这里记得设置，不然滚一次就停止了
                if (osTop == 0) {
                    clearInterval(timer);
                }
            }, 30);
        };
    }

    document.onmousemove = function lim(ev) {
        var bo = document.getElementsByTagName('body');
        var oEvent = ev || event;
        disX = oEvent.clientX - bo.offsetLeft;
        disY = oEvent.clientY - bo.offsetTop;

        var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        var y = oEvent.clientY;
        if (y < 0) { y = 0; }
    }
}