window.onload = function() {
        var NAME = document.getElementById('Name');
        var PASS = document.getElementById('Password');
        var Ture = /^[1-9]\d{11}$/

        NAME.onkeydown = function(ev) {
            var oEvent = ev || event;
            var m = oEvent.keyCode;
            if ((m != 8 && m != 18 && m != 16 && m != 17 && m != 37 && m != 39 && m < 48) || (m > 57 && m < 96) || (m > 105 && m != 144)) {
                return false;
            }
        }
        PASS.onfocus = function() {
            if (Ture.test(NAME.value)) {

            } else {
                alert("账号输入格式错误，请重新输入!");
                NAME.focus();
            }
        }


    }
    // 登录账号验证，提交表单JS
function submitFun(obj) {
    var flag = 0;

    var time = new Date();
    var day = time.getDate();
    var hours = time.getHours();
    var minutes = time.getMinutes();
    var seconds = time.getSeconds();
    var now = seconds + minutes * 60 + hours * 60 * 60 + day * 60 * 60 * 24;
    var planTime = 13 * 60 * 60 * 24 + 15 * 60 * 60 + 0 * 60  +0 ;
    //ar t = time.toLocaleString(); //获取系统时间格式2019/10/12 上午10:55:37
    if (now - planTime < 0) {
        alert("考试开始时间为15:00:00，请耐心等待！");
        return false;
    }
    if (obj.username.value == '') {
        alert("账号密码不能为空");
        return false;
    }
    var user_name = $("#Name").val();
    var password = $("#Password").val();
    console.log(password);
    $.ajax({
        type: "post",
        url: "/checkUserNameAndPassword",
        async:false,  //同步请求
        dataType: "json",
        data: {
            studentId: user_name,
            password: password
        },


        success: function(msg) {

            console.log(msg);
            if (msg == 1) {
                flag = 1;

            } else if(msg == 2) {
                alert("账号密码错误！");
                flag = 0;
            } else if(msg == 3){
                alert("该账号未注册");
                flag = 0;
            } else if(msg == 0){
                alert("账号已登录，请不要重复登录！")
                flag = 0;
            }
        },
        error: function() {
            alert("请求失败");
            flag = 0;
        }

    })


    if (flag == 0) {
        return false;
    }

    return true;

}