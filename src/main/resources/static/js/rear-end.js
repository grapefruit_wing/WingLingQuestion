$.ajax({
   type:"get",
   dataType:"json",
    url:"/result",
    data:{
    },
    success: function(data)
    {
        //选择题排名
        var ChoiceResult=$("#body1");
        ChoiceResult.html("");
        var str = "";
        var i=1;
        $.each(data['UserSScore'],function(index,obj){
            str +='<td id="f">'+(i++)+'</td>';
            str +='<td>'+obj['studentId']+'</td>';
            str +='<td>'+obj['smallScore']+'</td>';
        })
        ChoiceResult.html(str);
        ChoiceResult.append();

        //大题排名
        var BigResult=$("#body2");
        BigResult.html("");
        var str = "";
        var i=1;
        $.each(data['UserBScore'],function(index,obj){
            str +='<td id="f">'+(i++)+'</td>';
            str +='<td>'+obj['studentId']+'</td>';
            str +='<td>'+obj['bigScore']+'</td>';
        })
        BigResult.html(str);
        BigResult.append();

        //总分排名
        var ScoreResult = $("#body3");
        ScoreResult.html("");
        var str = "";
        var i=1;
	    $.each(data['UserScore'], function(index,obj) {
            str +='<td id="f">'+(i++)+'</td>';
            str +='<td>'+obj['studentId']+'</td>';
            str +='<td>'+obj['score']+'</td>';
        })
        ScoreResult.html(str);
        ScoreResult.append();  
        
    },
    error:function()
    {
        alert("请求失败");
    }
})
//大题获得题目
$.ajax({
    type:"get",
    dataType:"json",
    url:"/abq",
    data:{
    },
    success:function(data){

        /*添加大题题目*/
      var Question=$("#question");
        Question.html("");
        var str = " ";
        $.each(data['question'],function(index,obj){
            str+='<p>'+obj['bigquestion']+'</p>';
        })
        Question.html(str);
        Question.append();
        /**添加题号 */
        /*var i = 0;
        var Order = $("#order");
        Order.html("");
        var str = ' ';
        var i = 1;
        /* $.each(data['question'],function (index,obj) {
             str+='<nav queId="'+obj['id']+'">'+(i++)+'</nav>';
         })*/
       /* $.each(data['question'], function (index, obj) {
            str += '<nav queId="' + obj['id'] + '">' + '<p class="oa">' + (i++) + '</p>' + '</nav>';
        })
        Order.html(str);
        Order.append();*/
    },
    error:function()
    {
        alert("请求失败");
    }
})

//大题正确答案
$.ajax({
    type:"get",
    dataType:"json",
    url:"/aba",
    data:{
    },
    success:function(data){
        var Canswer=$("#canswer");
        Canswer.html("");
        var str = "";
        $.each(data['answers'],function(index,obj){
            str+='<textarea>'+obj['essayAnswers']+'</textarea>';
        })
        Canswer.html(str);
        Canswer.append();
    } 
})
window.onload=function() {
    //题目切换栏使用到的js
    var oUlSmall = document.getElementById('ul-one');
    var oLiSmall = oUlSmall.getElementsByTagName('li');

    var oUlBig = document.getElementById('ul-two');
    var oLiBig = oUlBig.getElementsByTagName('li');
    for (var i = 0; i < oLiSmall.length; i++) {
        oLiSmall[i].index = i;
        oLiSmall[i].onclick = function () {
            for (var i = 0; i < oLiSmall.length; i++) {
                oLiSmall[i].className = ' ';
                oLiBig[i].style.display = 'none';
            }
            oLiSmall[this.index].className = 'active';
            oLiBig[this.index].style.display = 'block';
        }
    }

    //从后端读取学生大题的做题结果的分页
    $(document).ready(function () {
        ajaxTest(1);

        function ajaxTest(num) {
            $.ajax(
                {
                    url: "/mark",
                    type: "get",
                    async: false,
                    data:
                        {
                            pageNum: num,
                        },
                    dataType: "json",
                    success: function (data) {
                        var i = 0;
                        var Order = $("#order");
                        Order.html("");
                        var str = ' ';
                        var i = 1;
                        $.each(data['userBAnswer'], function (index, obj) {
                            $.each(obj,function(index,arr){
                                str += '<nav queId="' + arr['id'] + '">' + '<p class="oa">' + (i++) + '</p>' + '</nav>';
                            })
                        })
                        Order.html(str);
                        Order.append();
                        //学生答案
                        console.log(data)
                        var Sanswer = $("#sanswer");
                        Sanswer.html("");
                        var str = ' ';
                        $.each(data['userBAnswer'], function (index, obj) {
                            $.each(obj, function (index, arr) {
                                console.log(arr['essayAnswers'])
                                if(arr['essayAnswers']==null||arr['essayAnswers']=='')
                                {
                                    str += '<textarea queid="'+arr['tag']+'">' + '这个学生这道题没有做'+ '</textarea>';
                                }
                                else
                                //str += '<p queid="'+arr['tag']+'">' + arr['essayAnswers'] + '</p>';
                                    str += '<textarea queid="'+arr['tag']+'">'+arr['essayAnswers']+'</textarea>\n'

                            })
                        })
                        Sanswer.html(str);
                        Sanswer.append();

                        //添加成绩
                          var Ss = $("#S");
                            Ss.html("");
                            var str = ' ';
                            $.each(data['userBAnswer'], function (index, obj) {
                                $.each(obj, function (index, arr) {
                                    console.log(arr['score']);
                                    if(arr['tag']==1) {
                                        str += '<input id="score" type="text" queid="' + arr['tag'] + '" value="' + arr['score'] + '" />';
                                    }
                                    else
                                    {
                                        str +='<input id ="score" type="text" queid="' + arr['tag'] + '" value=" " />';
                                    }
                                })
                            })
                            Ss.html(str);
                            Ss.append();
                        //学生学号
                        var StudentId = $("#ID");
                        $.each(data['userBAnswer'], function (index, obj) {
                            $.each(obj, function (index, arr) {
                                StudentId.val(arr['studentId']);
                            })
                        })
                        var oDiv1 = document.getElementById('order');
                        var aNav1 = oDiv1.getElementsByTagName('nav');

                        //题目
                        var oDiv2 = document.getElementById('question');
                        var ap2 = oDiv2.getElementsByTagName('p');

                        //正确答案
                        var oDiv3 = document.getElementById('canswer');
                        var ap3 = oDiv3.getElementsByTagName('textarea');

                        //学生答案
                        var oDiv4 = document.getElementById('sanswer');
                        var ap4 = oDiv4.getElementsByTagName('textarea');

                        //批改成绩
                        var oDiv5 = document.getElementById('S');
                        var aIn = oDiv5.getElementsByTagName('input');

                        for (var i = 0; i < aNav1.length; i++) {
                            console.log(aNav1[i])
                            aNav1[i].index = i;

                            aNav1[i].onclick = function () {
                                for (var i = 0; i < ap3.length; i++) {
                                    aNav1[i].className = ' ';
                                    aNav1[i].id = '';
                                    ap2[i].style.display = 'none';
                                    ap3[i].style.display = 'none';
                                    ap4[i].id = '';
                                    ap4[i].style.display = 'none';
                                    aIn[i].id = '';
                                    aIn[i].style.display = 'none';
                                }
                                aNav1[this.index].className = 'color';
                                aNav1[this.index].id = 'back';
                                ap2[this.index].style.display = 'block';
                                ap3[this.index].style.display = 'block';
                                ap4[this.index].style.display = 'block';
                                ap4[this.index].id = 'ajust';
                                aIn[this.index].id = 'score';
                                aIn[this.index].style.display = 'inline-block';
                            }
                        }

                            //分页
                        $("#newsPage").paging(
                            {
                                pageNo: num,
                                totalPage: data.size,
                                totalSize: 1,//改
                                callback: function (num) {
                                    ajaxTest(num)
                                }
                            })

                    },
                    error: function () {
                        alert('传送失败');
                    }
                })
        }
    })
}


    /*向后端传送每个学生的每道题的分数*/


    $("#YES").click(function ()
    {
        var questionnumber = $("#back").attr("queId")
        console.log(questionnumber)
        var studentId = $('#ID').val()
        var score = $("#score").val()
            if(score==null||score==''){
                alert('请打分');
                return;
            }
            else {
                ajaxTest(1);

                function ajaxTest(num) {
                    $.ajax(
                        {
                            url: "/updateScore",
                            type: "post",
                            async: false,
                            //contentType:'application/json',
                            data: {
                                id: questionnumber,
                                studentId: studentId,
                                score: score,
                            },
                            dataType: "json",
                            success: function () {
                                alert("批改完成");
                            },
                            error: function () {
                                alert('请打分');
                            }
                        })
                }
            }
    })

/*$("#score").click(function(){
        var Ajust=$("#ajust").attr("queid");
        if(Ajust==1){
            //alert("已经批改完成");
           $("#score").blur();
        }
        else{
            ;
        }
    }
)*/

