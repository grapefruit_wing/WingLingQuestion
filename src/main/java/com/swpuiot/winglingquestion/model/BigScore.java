package com.swpuiot.winglingquestion.model;

import lombok.Data;

@Data
public class BigScore {
    String studentId;
    int id;
    int tag;
    double score;
}
