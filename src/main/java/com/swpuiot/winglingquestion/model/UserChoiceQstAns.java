package com.swpuiot.winglingquestion.model;

import lombok.Data;

@Data
public class UserChoiceQstAns {
    private String studentId;
    private String choiceQstId;
    private char choiceQstAns;
    private int tag;
}
