package com.swpuiot.winglingquestion.model;

import lombok.Data;

@Data
public class ChoiceQstInfo {
    private int choiceQstId;
    private String choiceQstExp;
    private String A;
    private String B;
    private String C;
    private String D;

}
