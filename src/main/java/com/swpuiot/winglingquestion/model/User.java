package com.swpuiot.winglingquestion.model;

import lombok.Data;



@Data
public class User {

    private String studentId;
    private String password;
    private double smallScore;
    private double bigScore;
    private double score;
    private String role;
    private int flag;
    private int mark;

}
