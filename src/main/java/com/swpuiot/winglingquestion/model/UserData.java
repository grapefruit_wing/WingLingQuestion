package com.swpuiot.winglingquestion.model;

import lombok.Data;

@Data
public class UserData {
    String studentId;
    int id;
    double score;
    int tag;
}
