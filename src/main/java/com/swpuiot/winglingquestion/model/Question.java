package com.swpuiot.winglingquestion.model;

import lombok.Data;

@Data
public class Question {

    String bigquestion;
    int id;

    public String getBigquestion() {
        return bigquestion;
    }

    public void setBigquestion(String bigquestion) {
        this.bigquestion = bigquestion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
