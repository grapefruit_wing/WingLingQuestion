package com.swpuiot.winglingquestion.model;

import lombok.Data;

@Data
public class Answer {
    int id;
    String essayAnswers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
