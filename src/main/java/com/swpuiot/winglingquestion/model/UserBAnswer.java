package com.swpuiot.winglingquestion.model;

import lombok.Data;

import java.sql.Clob;

@Data
public class UserBAnswer {
    String studentId;
    String essayAnswers;
    int id;
    int tag;
    double score;
    int flag;
}
