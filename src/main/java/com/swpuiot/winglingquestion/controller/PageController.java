package com.swpuiot.winglingquestion.controller;


import com.swpuiot.winglingquestion.service.Impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


@Controller
public class PageController {


    @GetMapping("/")
    public String log(){
        return "denglu";
    }
//
    @GetMapping("/ending")
    public String log1(){
        return "ending";
    }

    @Autowired
    UserServiceImpl userService;

    @GetMapping("/login")
    public String login(){
        return "denglu";
    }

    @PreAuthorize("hasAnyRole('user')")
    @GetMapping("/dati")
    public String logSuccess(@AuthenticationPrincipal Principal principal){
//        String name = principal.getName();
//        System.out.println(name);
        return "dati";
    }

    @GetMapping("/prompt")
    public String doAnswer(){
        return "prompt";
    }

    @GetMapping("/sort")
    @CrossOrigin
    public String sort(){
        return "rear-end";
    }

    @GetMapping("/session/invalid")
    public String sessionInvalid(@AuthenticationPrincipal Principal principal){

        return "invalid";
    }
}
