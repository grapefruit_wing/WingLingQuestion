package com.swpuiot.winglingquestion.controller;


import com.swpuiot.winglingquestion.model.ChoiceQstInfo;
import com.swpuiot.winglingquestion.model.User;
import com.swpuiot.winglingquestion.model.UserData;
import com.swpuiot.winglingquestion.service.Impl.AnswerServiceImpl;
import com.swpuiot.winglingquestion.service.Impl.QuestionServiceImpl;
import com.swpuiot.winglingquestion.service.Impl.UserBAnswerServiceImpl;
import com.swpuiot.winglingquestion.service.Impl.UserServiceImpl;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private UserBAnswerServiceImpl userBAnswerService;
    @Autowired
    private QuestionServiceImpl questionService;
    @Autowired
    private AnswerServiceImpl answerService;

    @GetMapping("/showChoQst")
    public Map showChoiceQst(){
        List<ChoiceQstInfo> list = userService.findAllChoiceQsts();
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("result", list);
        returnMap.put("status", 200);
        return returnMap;
    }



    @PostMapping("/checkUserNameAndPassword")
    public int checkUserNameAndPassword(@RequestParam("studentId") String studentId,
                                        @RequestParam("password") String password){

        User user  = userService.getUser(studentId);

        //返回值 是 1，表示正确登录，2表示账号密码错误, 3表示用户不存在, 0表示已经登录过
        //4 表示其他情况
        if(user == null){

            return 3;
        }
        else{
            if(user.getPassword().equals(password)){

                if(userService.selectMark(studentId) == 1){

                    return 1;
                }
                else if(userService.selectMark(studentId) == 0){
                    return 0;
                }

            }else{
                return 2;
            }
        }
        return 4;
    }

    @PostMapping("/getAns")
    public boolean submitAns(@RequestBody Map<String,List<Map<String,Object>>> map,
                             @AuthenticationPrincipal Principal principal) {

        String studentId = principal.getName();
        List<Map<String, Object>> choiceList = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> essayList = new ArrayList<Map<String, Object>>();
        Map<String,Object> choiceMap = new HashMap<>();
        Map<String,Object> essayMap = new HashMap<>();
        Map<String,Object> idMap = new HashMap<>();
        idMap.put("studentId",studentId);


        for (Map.Entry<String, List<Map<String, Object>>> m : map.entrySet()) {
            if (m.getKey().equals("choiceAnswers")) {
                choiceList = m.getValue();
                choiceList.add(idMap);

            }
            if (m.getKey().equals("essayAnswers")) {
                essayList = m.getValue();


            }

        }
//        System.out.println(choiceList);
//        System.out.println(essayList);

        for(int i=0;i<choiceList.size();i++){
            choiceMap.putAll(choiceList.get(i));
        }


//        System.out.println(choiceMap);


        if(userService.insertUserChoQstAnsByMap(choiceMap)) {

            for(int i=0;i<essayList.size();i++){
                essayMap.putAll(essayList.get(i));
                essayMap.put("studentId",studentId);
//                System.out.println(essayMap);
                userService.insertUserAnsByMap(essayMap);
            }


        }
        else {
            return false;
        }

        if(userService.selectMark(studentId) == 1){
            userService.updateMark(studentId);
            return true;
        }
        return false;
    }


    @GetMapping("/result")
    @CrossOrigin
    //@CrossOrigin
    public JSONObject getUserSort(){
        return userService.getUserScore();
    }

    @GetMapping("/mark")
    @CrossOrigin
    public Map mark(Model model, @RequestParam(defaultValue = "1",value = "pageNum")int pageNum, @RequestParam(defaultValue = "1",value = "pageSize")int pageSize){
        return userBAnswerService.getUserAnswer(model,pageNum,pageSize);
    }
    @GetMapping("/abq")
    //@CrossOrigin
    public JSONObject getAllBQuestions(){
        return questionService.getquestions();
    }

    @GetMapping("/aba")
    public JSONObject getAllBAnswrs(){
        return answerService.getBAnswrs();
    }

    @PostMapping("/updateScore")
    //@CrossOrigin
    public int getScore(UserData userData){
        userService.getscore(userData.getStudentId(),userData.getId(),userData.getScore());
        return 1;
    }



}
