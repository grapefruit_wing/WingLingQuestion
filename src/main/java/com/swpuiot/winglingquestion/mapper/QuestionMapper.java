package com.swpuiot.winglingquestion.mapper;

import com.swpuiot.winglingquestion.model.Question;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface QuestionMapper {

    @Select("select * from question where id>4")
    public List<Question> getAllBQuestions();

    @Select("select * from question where id=#{id}")
    public Question getBQuestion(int id);

}
