package com.swpuiot.winglingquestion.mapper;

import com.swpuiot.winglingquestion.model.BigScore;
import com.swpuiot.winglingquestion.model.UserBAnswer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BigScoreMapper {

    @Select("select score,tag from userbanswer where (studentId=#{studentId} and id=#{id})")
    public BigScore getBigScore(@Param("studentId") String studentId, @Param("id") int id);
}
