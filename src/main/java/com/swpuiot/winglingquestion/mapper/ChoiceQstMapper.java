package com.swpuiot.winglingquestion.mapper;

import com.swpuiot.winglingquestion.model.ChoiceQstInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ChoiceQstMapper {

    @Select("select * from choice_right_ans")
    public Map<String,Object> findAllRightChoiceAns();

    @Select("select * from user_choice_ans")
    public List<Map<String,Object>> findAllUserChoiceAns();

    @Insert("update user set smallscore=#{smallscore} where studentId=#{studentId} ")
    public boolean updateChoiceScore(Map<String,Object> map);

    @Select("select * from choice_qst_info")
    public List<ChoiceQstInfo> findAllChoiceQsts();

}
