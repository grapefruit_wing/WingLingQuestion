package com.swpuiot.winglingquestion.mapper;


import com.swpuiot.winglingquestion.model.Answer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AnswerMapper {

    @Select("select * from answer ")
    public List<Answer> getAllBAnswers();

    @Select("select * from answer where id=#{id}")
    public Answer getBAnswer(int id);
}
