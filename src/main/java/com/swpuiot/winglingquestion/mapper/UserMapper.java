package com.swpuiot.winglingquestion.mapper;

import com.swpuiot.winglingquestion.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserMapper {

    @Select("select * from user where studentId=#{username}")
    public User select( String  studentId);

    @Select("select * from user order by smallscore desc")
    public List<User> getUserSScore();

    @Select("select * from user order by bigscore desc")
    public List<User> getUserBScore();

    @Select("select * from user order by score desc")
    public List<User> getUserScore();

    @Select("select * from user where studentId=#{studentId}")
    public User getUser(String studentId);

    @Select("select * from user")
    public List<User> AllUser();

    @Update("update user set bigscore=#{bigScore},score=#{score} where studentId=#{studentId}")
    public int update(User user);

    @Update("update user set flag=1 where studentId=#{studentId}")
    public int updateFlag(String studentId);

    @Select("select * from user where flag=1")
    public List<User> getUserList();

    @Update("update user set flag=0 ")
    public int setFlagZero();

    @Update("update user set score=#{score} where studentId=#{studentId}")
    public int updateFinalScore(User user);

    @Select("select mark from user where studentId=#{studentId}")
    public int selectMark(String studentId);

    @Update("update user set mark=1 where studentId=#{studentId}")
    public boolean updateMark(String studnetId);
}
