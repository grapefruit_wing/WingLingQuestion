package com.swpuiot.winglingquestion.mapper;


import com.swpuiot.winglingquestion.model.UserBAnswer;
import com.swpuiot.winglingquestion.model.UserChoiceQstAns;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface UserAnswerMapper {


    @Insert("insert into user_choice_ans (studentId,choice_qst_id,choice_qst_ans) " +
            "values (#{studentId},#{choiceQstId},#{choiceQstAns})")
    public boolean insertUserChoQstAns(UserChoiceQstAns choiceQstAns);

    @Insert("insert into user_choice_ans (studentId,ans_one,ans_two,ans_three,ans_four,ans_five,ans_six,ans_seven) " +
            "values (#{studentId},#{optQuestion0},#{optQuestion1},#{optQuestion2},#{optQuestion3},#{optQuestion4},#{optQuestion5},#{optQuestion6})" )
    public boolean insertUserChoQstAnsByMap(Map<String,Object> map);

    @Insert("insert into userbanswer (studentId,essayAnswers,id)" +
            "values (#{studentId},#{wAnswer},#{id})" )
    public boolean insertUserAnsByMap(Map<String,Object> map);

    @Select("select studentId,essayAnswers,id,score,tag from userbanswer where studentId=#{studentId}")
    public List<UserBAnswer> getBAnswer(String studentId);

    @Select("select tag from userbanswer where studentId=#{studentId} and id=#{id}")
    public int getTag(@Param("studentId") String studentId, @Param("id") int id);

    @Select("select studentId,id from userbanswer")
     public List<UserBAnswer> getAllUserAnswer();

    @Update("update userbanswer set tag=1,score=#{score} where studentId=#{studentId} and id=#{id}")
    public int updateUserBAnswer(@Param("score") double score, @Param("studentId") String studentId, @Param("id") int id);


    /*@Select("select * from user_ban_ans")
    public List<UserBAnswer> getAllUserBAnswer();

    @Insert("<script>insert into userbanswer (studentId,id,answer,tag,score)" + "values" +
            "<foreach collection='list' item='userBAnswer' separator=','>" +
               "(#{userBAnswer.studentId},#{userBAnswer.id},#{userBAnswer.answer},#{userBAnswer.tag},#{userBAnswer.score})" +
               "</foreach></script>")
    public int putUserBAnswers(List<UserBAnswer> userBAnswers);*/

    /*@Insert("<script>insert into image(url,type,tag,description)" +
            "   values " +
            "<foreach collection='list' item='item' separator=','>" +
            "(#{item.url},#{item.type},#{item.tag},#{item.description})" +
            "</foreach></script>")*/

}
