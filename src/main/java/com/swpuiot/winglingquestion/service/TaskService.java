package com.swpuiot.winglingquestion.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Map;

@Service
public interface TaskService {

    public void  taskScheduled();

}
