package com.swpuiot.winglingquestion.service;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Map;

@Service
public interface ChoiceAnsComparison {

    public Map<String,Object> findAllChoiceRightAns();

    public List<Map<String,Object>> findUserAllChoiceAns();

    public boolean comparisonAns(List<Map<String,Object>> listMap, Map<String,Object> ansMap);

    public boolean updateUserScore(Map<String,Object> map);


}
