package com.swpuiot.winglingquestion.service;

import com.swpuiot.winglingquestion.mapper.UserMapper;
import com.swpuiot.winglingquestion.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
public class MyUserService  implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;

    private Collection<GrantedAuthority> getAuthorities(User user){
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        authList.add(new SimpleGrantedAuthority("ROLE_"+user.getRole()));
        return authList;
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User userByName = userMapper.select(s);

        UserDetails userDetails = null;
        if(userByName != null){
            Collection<GrantedAuthority> authorities = getAuthorities(userByName);
            userDetails = new org.springframework.security.core.userdetails.User(s,userByName.getPassword(),true,true,true,true,authorities);

        }
        else{
            System.out.println("不存在该用户");
            throw new UsernameNotFoundException("用户名不存在");
        }
        return userDetails;
    }
}
