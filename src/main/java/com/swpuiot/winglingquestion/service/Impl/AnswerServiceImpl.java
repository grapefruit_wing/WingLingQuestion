package com.swpuiot.winglingquestion.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.swpuiot.winglingquestion.mapper.AnswerMapper;
import com.swpuiot.winglingquestion.model.Answer;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class AnswerServiceImpl {

    @Autowired
    AnswerMapper answerMapper;

    //所有标准答案
    public JSONObject getBAnswrs(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("answers",answerMapper.getAllBAnswers());
        return jsonObject;
    }


}
