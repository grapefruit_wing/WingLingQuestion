package com.swpuiot.winglingquestion.service.Impl;

import com.swpuiot.winglingquestion.controller.UserController;
import com.swpuiot.winglingquestion.mapper.ChoiceQstMapper;
import com.swpuiot.winglingquestion.service.ChoiceAnsComparison;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RestController
public class ChoiceAnsComparisonImpl implements ChoiceAnsComparison {

    @Autowired
    ChoiceQstMapper choiceQstMapper;


    @Override
    public Map<String, Object> findAllChoiceRightAns() {
        Map<String,Object> ansMap = new HashMap<>();
        ansMap.putAll(choiceQstMapper.findAllRightChoiceAns());

        if(ansMap!=null){
            return ansMap;
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> findUserAllChoiceAns() {
        List<Map<String,Object>> userAnsLisMap = new ArrayList<>();
        userAnsLisMap=choiceQstMapper.findAllUserChoiceAns();

        if(userAnsLisMap!=null){
            return userAnsLisMap;
        }
        return null;
    }

    @Override
    public boolean updateUserScore(Map<String,Object> map) {
        if(choiceQstMapper.updateChoiceScore(map)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean comparisonAns(List<Map<String,Object>> mapList,Map<String,Object> ansMap) {
        int score = 0;

        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        listMap = mapList;

        for (int i = 0; i < listMap.size(); i++) {

            Map<String,Object> userAnsMap = new HashMap<>();
            Map<String, Object> returnMap = new HashMap<>();
            ansMap.putAll(findAllChoiceRightAns());
            userAnsMap.putAll(listMap.get(i));

            for (Map.Entry<String, Object> entry1 : userAnsMap.entrySet()) {
                if (entry1.getKey().equals("studentId")) {
                    returnMap.put("studentId", entry1.getValue());
                    continue;
                }

                Object userValue = entry1.getValue() == null ? "" : entry1.getValue();
                Object ansValue = ansMap.get(entry1.getKey()) == null ? "" : ansMap.get(entry1.getKey());

                if (userValue.equals(ansValue)) {
                    score += 3;
                }

            }

            returnMap.put("smallscore", score);

            if (updateUserScore(returnMap)) {

                score=0;
                continue;

            }

            return false;

        }

        return true;
    }



}
