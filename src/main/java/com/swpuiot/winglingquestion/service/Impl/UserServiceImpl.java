package com.swpuiot.winglingquestion.service.Impl;

import com.swpuiot.winglingquestion.mapper.*;
import com.swpuiot.winglingquestion.model.BigScore;
import com.swpuiot.winglingquestion.model.ChoiceQstInfo;
import com.swpuiot.winglingquestion.model.User;
import com.swpuiot.winglingquestion.model.UserChoiceQstAns;
import com.swpuiot.winglingquestion.service.UserService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    AnswerMapper answerMapper;

    @Autowired
    private ChoiceQstMapper choiceQstMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserAnswerMapper userAnswerMapper;

    @Autowired
    UserBAnswerServiceImpl userBAnswerService;

    @Autowired
    BigScoreMapper bigScoreMapper;

    @Override
    public List<ChoiceQstInfo> findAllChoiceQsts() {
        return choiceQstMapper.findAllChoiceQsts();
    }

    @Override
    public User getUser(String username) {
        return userMapper.select(username);

    }

    @Override
    public boolean insertUserChoQstAns(UserChoiceQstAns userChoiceQstAns) {
        if(userAnswerMapper.insertUserChoQstAns(userChoiceQstAns)) {
            return true;
        }
        return false;
    }


    @Override
    public boolean insertUserChoQstAnsByMap(Map<String,Object> map) {
        if(userAnswerMapper.insertUserChoQstAnsByMap(map)){
            return true;
        }
        return  false;

    }

    @Override
    public boolean insertUserAnsByMap(Map<String, Object> map) {
        if(userAnswerMapper.insertUserAnsByMap(map)) {
            return true;
        }
        return false;
    }

    @Override
    public int selectMark(String studentId) {

        if(userMapper.selectMark(studentId) == 0){

            return 1;
        }
        return 0;
    }

    @Override
    public int updateMark(String studentId) {
        //返回值是 1 表示修改成功， 0 表示修改失败
        if(userMapper.updateMark(studentId)){
            return 1;
        }
        return 0;
    }

    public void updateFinalScore(){
        for (User user:userMapper.getUserList()){
            user.setScore(user.getBigScore()+user.getSmallScore());
            userMapper.updateFinalScore(user);
        }
    }


    //获得所有用户分数
    public JSONObject getUserScore(){
        updateFinalScore();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("UserSScore",userMapper.getUserSScore());
        jsonObject.put("UserBScore",userMapper.getUserBScore());
        jsonObject.put("UserScore",userMapper.getUserScore());
        return jsonObject;
    }




    //更新用户分数
    public double getscore(String studentId,int id,double score){
            User user = userMapper.getUser(studentId);
            if (userAnswerMapper.getTag(studentId,id) == 1){
                BigScore bigScore = bigScoreMapper.getBigScore(studentId,id);
                user.setBigScore(user.getBigScore() - bigScore.getScore());
                user.setScore(user.getScore() - bigScore.getScore());
                //return  bigScore.getScore();
            }
            user.setBigScore(user.getBigScore() + score);
            user.setScore(user.getScore() + score);
            userMapper.update(user);
            userAnswerMapper.updateUserBAnswer(score, studentId, id);
            return 1;

    }
}
