package com.swpuiot.winglingquestion.service.Impl;


import com.github.pagehelper.PageHelper;
import com.swpuiot.winglingquestion.mapper.*;
import com.swpuiot.winglingquestion.model.User;
import com.swpuiot.winglingquestion.model.UserBAnswer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@Service
public class UserBAnswerServiceImpl {

    @Autowired
    UserAnswerMapper userAnswerMapper;

    @Autowired
    QuestionMapper questionMapper;

    @Autowired
    AnswerMapper answerMapper;

    @Autowired
    UserMapper userMapper;


    //判断用户是否答题
    public List userList(){
        List usersBAnswerslist = new ArrayList<>();
        userMapper.setFlagZero();
        for(User user:userMapper.AllUser()){
            for(UserBAnswer userBAnswer:userAnswerMapper.getAllUserAnswer()) {
                if(user.getStudentId().equals(userBAnswer.getStudentId()) ) {
                    userMapper.updateFlag(user.getStudentId());
                    usersBAnswerslist.add(user.getStudentId());
                    break;
                }
            }
        }
        return usersBAnswerslist;
    }

    //获得所有已答题的用户
    public List usersAnswers(){
        List usersBAnswerslist = new ArrayList<>();
        for (User user:userMapper.getUserList()){
            List<UserBAnswer> answerList = userAnswerMapper.getBAnswer(user.getStudentId());
            usersBAnswerslist.add(answerList);
        }
        return usersBAnswerslist;
    }

    //获得所有用户答案
    public Map getUserAnswer(Model model,int pageNum,int pageSize){
        List list = this.userList();
        PageHelper.startPage(pageNum,pageSize);
        List useranswerslist = this.usersAnswers();
        Map<String,Object> map = new HashMap<>();
        map.put("userBAnswer",useranswerslist);//用户答案
        map.put("size",list.size());//用户总数
        model.addAttribute("userBAnswers",map);
        return map;
    }

}