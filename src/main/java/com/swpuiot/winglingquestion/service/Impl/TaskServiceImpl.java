package com.swpuiot.winglingquestion.service.Impl;


import com.swpuiot.winglingquestion.controller.UserController;
import com.swpuiot.winglingquestion.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


import java.text.SimpleDateFormat;
import java.util.Date;


@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    ChoiceAnsComparisonImpl choiceAnsComparison;

    @Autowired
    UserController userController;


//    @Scheduled(cron = "0 0/10 * * * *")
    @Scheduled(cron = "0 51 20 14 10 *")
//    @Scheduled(cron = "0 */10 * * * *")
    @Override
    public void taskScheduled() {
        Date d = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(df.format(d)+":"+"开始执行定时任务");
        choiceAnsComparison.comparisonAns(choiceAnsComparison.findUserAllChoiceAns(),
                                            choiceAnsComparison.findAllChoiceRightAns());
    }



}
