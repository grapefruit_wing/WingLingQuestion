package com.swpuiot.winglingquestion.service.Impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.swpuiot.winglingquestion.mapper.QuestionMapper;
import com.swpuiot.winglingquestion.model.Question;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@RestController
public class QuestionServiceImpl {

    @Autowired
    QuestionMapper questionMapper;

    //所有问题
    public JSONObject getquestions(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("question",questionMapper.getAllBQuestions());
        return jsonObject;
    }
}
