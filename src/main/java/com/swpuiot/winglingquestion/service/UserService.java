package com.swpuiot.winglingquestion.service;

import com.swpuiot.winglingquestion.model.ChoiceQstInfo;
import com.swpuiot.winglingquestion.model.User;
import com.swpuiot.winglingquestion.model.UserChoiceQstAns;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface UserService {
    public List<ChoiceQstInfo> findAllChoiceQsts();

    public User getUser(String StudentId);

    public boolean insertUserChoQstAns(UserChoiceQstAns userChoiceQstAns);

    public boolean insertUserChoQstAnsByMap(Map<String,Object> map);

   public boolean insertUserAnsByMap(Map<String,Object> map);

   public int selectMark(String studentId);

   public int  updateMark(String studentId);

}
